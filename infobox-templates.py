#!/usr/bin/python3

"""
Try to come up with a small set of templates, at least one of which is used
in any infobox.
"""

import argparse
import requests
import json
import locale
from string import Template
import urllib.parse

import mwapi


# helpers

template_ns_name = None

def is_template(title: str):
	"""Check if a title is in the Template namespace."""
	return title.startswith(template_ns_name + ':')

# globals

all_infobox_templates = []
infobox_building_block_templates = []
derived_infobox_templates = []


# CLI args

parser = argparse.ArgumentParser(description = __doc__)
parser.add_argument('wiki_language', type = str, help = 'Wikipedia language code')
parser.add_argument('--format', '-f', choices = ['search', 'links', 'json'],
	default = 'search', help = 'template list format')
args = parser.parse_args()


# load global data

ua = 'one-off/gtisza@wikimedia.org'
lang = args.wiki_language
wikipedia_session = mwapi.Session('https://%s.wikipedia.org/w/api.php' % lang, ua)
data = wikipedia_session.get(
	formatversion = 2,
	action = 'query',
	meta = 'siteinfo',
	siprop = 'namespaces',
)
template_ns_name = data['query']['namespaces']['10']['name']


# fetch infobox templates by looking at wikidata instance_of:Wikimedia_infobox properties

query = """
	select distinct ?sitelink where {
		?item wdt:P31 wd:Q19887878 .	
		{ ?sitelink schema:about ?item . ?sitelink schema:isPartOf <https://$LANG.wikipedia.org/> } .
		SERVICE wikibase:label { bd:serviceParam wikibase:language "$LANG" }
	}
"""

r = requests.post('https://query.wikidata.org/sparql',
	headers = {
		'Accept': 'application/sparql-results+json',
		'User-Agent': ua,
	},
	data = {
		'query': Template(query).substitute(LANG=lang),
	},
)
r.raise_for_status()
data = r.json()

for item in data['results']['bindings']:
	all_infobox_templates.append(
		urllib.parse.urlparse(urllib.parse.unquote(
			item['sitelink']['value']
		)).path.split('/', 2)[-1].replace('_', ' ')
	)


# fetch templates which use common building blocks

infobox_building_blocks = [
	'Q5626735', # {{Infobox}}
	'Q13107716', # Module:Infobox
	'Q11126342', # {{Infobox start}}
	'Q10563837', # cswiki {{Infobox start}}
	'Q101145716', # arwiki Module:Infobox
]
wiki = f'{lang}wiki'
wikidata_session = mwapi.Session('https://www.wikidata.org/w/api.php', ua)
for qid in infobox_building_blocks:
	data = wikidata_session.get(
		action = 'wbgetentities',
		ids = qid,
		props = 'sitelinks',
		sitefilter = wiki,
	)
	if wiki in data['entities'][qid]['sitelinks']:
		infobox_template_name = data['entities'][qid]['sitelinks'][wiki]['title']
		infobox_building_block_templates.append(infobox_template_name)


wikipedia_session = mwapi.Session('https://%s.wikipedia.org/w/api.php' % lang, ua)
continued = wikipedia_session.get(
	formatversion = 2,
	action = 'query',
	prop = 'transcludedin',
	titles = '|'.join(infobox_building_block_templates),
	tiprop = 'title',
	tinamespace = 10,
	tishow = '!redirect',
	tilimit = 'max',
	continuation = True,
)
for data in continued:
	for tpl in data['query']['pages']:
		for page in tpl.get('transcludedin', []):
			derived_infobox_templates.append(page['title'])


# keep the set minimal - templates using known building blocks don't need to be listed

misc_infobox_templates = set(all_infobox_templates) - set(derived_infobox_templates)

# expand templates to see the transclusion tree without <includeonly> messing it up

for tpl in set(misc_infobox_templates):
	if not is_template(tpl):
		continue
	data = wikipedia_session.get(
		action = 'parse',
		formatversion = 2,
		prop = 'templates',
		contentmodel = 'wikitext',
		text = '{{%s}}' % tpl,
	)
	for page in data['parse'].get('templates', []):
		if page['title'] in infobox_building_block_templates:
			misc_infobox_templates.discard(tpl)
			break


# done: use building blocks + non-building-block-based Wikidata-tracked infoboxes
search_infobox_templates = misc_infobox_templates | set(infobox_building_block_templates)
search_infobox_templates = sorted(search_infobox_templates, key=lambda t: str(not is_template(tpl)) + locale.strxfrm(t))


# output

print(str(len(search_infobox_templates)) + ' templates')
if args.format == 'search':
	print('|'.join([ '"{}"'.format(tpl) for tpl in search_infobox_templates]))
elif args.format == 'links':
	for tpl in search_infobox_templates:
		print(f'https://{lang}.wikipedia.org/wiki/{tpl}'.replace(' ', '_'))
elif args.format == 'json':
	print(json.dumps(
		search_infobox_templates,
		ensure_ascii = False,
		indent = ' '*8,
	))
